{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NamedFieldPuns #-}

module Data.Types where

import qualified Data.Entities as GGS

import Data.Int
import Data.Foldable
import Data.Traversable
import Control.Applicative

import qualified Data.Time as Time
import Data.Text (Text)
import qualified Data.Text as T
import Data.ByteString.Char8 (ByteString)
import qualified Data.ByteString.Char8 as BS
import Data.Vector (Vector)
import qualified Data.Vector as V
import qualified Data.HashMap.Strict as HM
import Data.Csv
import Database.Persist.Sql

type Slug = Text

data Score = Score
	{ slug :: !Slug
	, name :: !Text
	, days :: !Days
	} deriving (Show)

newtype Days = Days { unDays :: Vector (Maybe Day) }
	deriving (Show)

data Day = Day
	{ score :: !Text
	, place :: !Place
	, youtubeId :: !(Maybe Text)
	, matchName :: !(Maybe Text)
	, notes :: !(Maybe Text)
	} deriving (Show)

newtype Place = Place { fromPlace :: Int32 }
	deriving (Show, Eq, Ord, Num)

data EventGame = EventGame
	{ slug :: !Slug
	, gameName :: !Text
	, eventName :: !Text
	} deriving (Show)

instance FromField Place where
	parseField s = parseField @Text s >>= \case
		"first"  -> pure 1
		"second" -> pure 2
		"third"  -> pure 3
		_ -> fail "not first, second, or third"

instance FromNamedRecord Score where
	parseNamedRecord m = Score <$> m .: "slug" <*> m .: "name" <*> parseNamedRecord m

instance FromNamedRecord Days where
	parseNamedRecord m = Days <$> for (V.generate 7 (+1)) parseDay where
		parseDay :: Int -> Parser (Maybe Day)
		parseDay day =
			( Just
				<$> ( Day
					<$> m .: col "score"
					<*> m .: col "place"
					<*> m .: col "youtube"
					<*> (fmap stripSuroundingParens <$> m .: col "name")
					<*> m .: col "notes"
					)
			)
			<|> pure Nothing
			where
				stripSuroundingParens :: Text -> Text
				stripSuroundingParens = T.dropWhileEnd (== ')') . T.dropWhile (== '(')

				col :: ByteString -> ByteString
				col colName = "days." <> BS.pack (show day) <> "." <> colName

instance FromNamedRecord EventGame where
	parseNamedRecord m = do
		theSlug <- m .: "slug"
		theEventName <- m .: "name"
		let
			gameNameIfParenExists = T.stripEnd . T.dropWhileEnd ((==) '(') . fst $ T.breakOnEnd "(" theEventName
			theGameName = if T.null gameNameIfParenExists then theEventName else gameNameIfParenExists
		return $ EventGame
			{ slug = theSlug
			, gameName = theGameName
			, eventName = theEventName
			}


data FoldStateEventGame = FoldStateEventGame
	{ lastEndTime :: !Time.UTCTime
	, slugAndEntities :: ![(Slug, GGS.Game, GGS.Event)]
	}

-- Returns the slug and events with likely duplicated games.
eventGamesToEntities
	:: Foldable f
	=> Time.UTCTime -- ^ end time of the latest record,
	           -- used to calculate start and end times of following records by assuming each is a week long
	-> f EventGame
	-> [(Slug, GGS.Game, GGS.Event)] -- event game ids are not set as the game in fst needs to be inserted first
eventGamesToEntities latestEndTime = slugAndEntities . foldl' go initVal where
	weekAgo = Time.nominalDay * (-7)
	sixDaysAgo = Time.nominalDay * (-6)
	initVal = FoldStateEventGame latestEndTime []
	toSlugAndEntites endTime (EventGame theSlug theGameName theEventName) =
		( theSlug
		, GGS.Game
			{ gameName = theGameName
			}
		, GGS.Event
			{ eventName = theEventName
			-- needs to be filled in by the code that inserts the associated game above
			, eventGame = toSqlKey (-1)
			, eventStartsOn = Time.addUTCTime sixDaysAgo endTime
			, eventEndsOn = endTime
			, eventMatchesExpected = 7
			-- Descriptions are in the footnotes folder named <slug>.md.
			-- As they are not easily obtainable in the csvs,
			-- theyll be added manually for now as there are only a couple right now.
			-- In the future, the lib github, can be used to fetch the latest commit,
			-- traverse the tree objects to the folder and then fetch the blobs
			-- to obtain the latest descriptions if this ever becomes a cron job.
			, eventDescription = Nothing
			}
		)
	go (FoldStateEventGame endTime entities) eventGame =
		FoldStateEventGame (Time.addUTCTime weekAgo endTime) $ toSlugAndEntites endTime eventGame : entities

-- | Returns Match and entry per day.
-- There will be duplicate matches. Some will have a name set.
-- Matches need to be conditionally inserted (INSERT ... ON CONFLICT DO NOTHING, upsert)
-- and one with names upserted (INSERT ... ON CONFLICT DO UPDATE ...) (since postgres 9.3).
scoreToEntities :: (Functor f, Foldable f) => HM.HashMap Slug GGS.EventId -> f Score -> [(GGS.Match, GGS.Entry)]
scoreToEntities slugsToEventId = concat . fmap (V.toList . scoreConvert) where
	scoreConvert Score{slug, name, days} = V.imapMaybe daysConvert $ unDays days where
		daysConvert i = fmap dayConvert where
			dayConvert Day{score, place, youtubeId, matchName, notes} =
				( GGS.Match
					{ matchEventId = slugsToEventId HM.! slug
					, matchMatch = fromIntegral i
					, matchName = matchName
					}
				, GGS.Entry
					{ entryEventId = slugsToEventId HM.! slug
					, entryMatch = fromIntegral i
					, entryContestantHandle = nameToHandle name
					, entryResult = score
					, entryRank = Just $ fromPlace place
					, entryPoints = Nothing
					, entryNote = notes
					, entryYoutubeVideo = fmap ("https://youtu.be/" <>) youtubeId
					, entryPublicAfter = Nothing
					}
				)

nameToHandle :: Text -> Text
nameToHandle = \case
	-- These contestants with these handles must be insterted manually before inserting entries
	"Dan Gheesling" -> "DanGheesling"
	"Michael Al Fox" -> "michaelalfox"
	"Northernlion" -> "Northernlion"
	name -> error $ "unkown name," <> T.unpack name  <> ", in data"
