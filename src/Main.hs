{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import Data.Types
import qualified Data.Entities as GGS

import Data.Traversable
import Data.Foldable
import Control.Monad
import System.Environment

import Data.Csv
import Data.Time
import qualified Data.ByteString.Lazy.Char8 as BL
import qualified Data.ByteString.Char8 as B
import qualified  Data.HashMap.Strict as HM
import Database.Persist
import Database.Persist.Postgresql
import Control.Monad.Logger
import Control.Monad.IO.Unlift

main :: IO ()
main = do
	(_, games) <- either fail return =<< decodeByName @EventGame <$> BL.readFile "games.csv"
	(_, scores) <- either fail return =<< decodeByName @Score <$> BL.readFile "score.csv"

	mDbUrl <- lookupEnv "DATABASE_URL"
	let
		connStr = maybe "postgresql://test:password@localhost" B.pack mDbUrl
		gamesEndTime = UTCTime (fromGregorian 2020 11 25) (secondsToDiffTime 0)
		logFilePath = "gg-net-scraper.log"

	putStrLn $ maybe "Using test db url" (const "Using db url in $DATABASE_URL") mDbUrl
	putStrLn $ "Appending logs to " <> logFilePath

	runFileLoggingT logFilePath $ withPostgresqlConn connStr $ \conn -> do
		slugsToEventIds <- insertEventGames conn $ eventGamesToEntities gamesEndTime games
		insertContestants conn
		insertScores conn $ scoreToEntities slugsToEventIds scores

insertEventGames
	:: MonadUnliftIO m
	=> SqlBackend
	-> [(Slug, GGS.Game, GGS.Event)]
	-> m (HM.HashMap Slug GGS.EventId)
insertEventGames conn slugsAndEntities =
	flip runSqlConn conn $ do
		gameIds <- for games $ \game -> entityKey <$> upsert game []
		let events = zipWith (\gameId event -> event { GGS.eventGame = gameId }) gameIds eventsWithoutGameIds
		eventIds <- insertMany events
		return . HM.fromList $ zip slugs eventIds
	where
		(slugs, games, eventsWithoutGameIds) = unzip3 slugsAndEntities

insertScores
	:: MonadUnliftIO m
	=> SqlBackend
	-> [(GGS.Match, GGS.Entry)]
	-> m ()
insertScores conn =
	flip runSqlConn conn
	-- entries have an FK to matches so insert matches first
	. (\(matches, entries) -> do
		for_ matches $ \match ->
			rawExecute
				( "INSERT INTO match (event_id, match, name)"
				<> " VALUES (?, ?, ?)"
				<> " ON CONFLICT (event_id, match)"
				<> " DO UPDATE SET name = COALESCE(EXCLUDED.name, match.name)"
				)
				(toPersistValue <$> toPersistFields match)
		void $ insertMany entries
	)
	. unzip

insertContestants
	:: MonadUnliftIO m
	=> SqlBackend
	-> m ()
insertContestants conn = flip runSqlConn conn . void $ insertMany baseContestants
	where
		baseContestants =
			[ GGS.Contestant
				{ GGS.contestantHandle = "DanGheesling"
				, GGS.contestantName = Just "Dan Gheesling"
				, GGS.contestantYoutubeChannel = Just "https://www.youtube.com/c/DanGheesling"
				}
			, GGS.Contestant "Northernlion" (Just "Ryan Letourneau") (Just "https://www.youtube.com/user/Northernlion")
			, GGS.Contestant "michaelalfox" (Just "Michael Al Fox") (Just "https://www.youtube.com/user/michaelalfox")
			]
